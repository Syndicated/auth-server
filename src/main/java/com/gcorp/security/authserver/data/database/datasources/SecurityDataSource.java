package com.gcorp.security.authserver.data.database.datasources;

import com.gcorp.security.authserver.properties.DatabaseProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;

public class SecurityDataSource  {


    protected String url;
    protected String driverClassName;
    protected String username;
    protected String password;


    @Autowired
    DatabaseProperties databaseProperties;

    public SecurityDataSource(DatabaseProperties databaseProperties ) {

        this.url = databaseProperties.getSql().getUrl();
        this.driverClassName = databaseProperties.getSql().getDriverClassName();
        this.username = databaseProperties.getSql().getUsername();
        this.password = databaseProperties.getSql().getPassword();

        createAndBuildDataSource(databaseProperties);

    }


    public DataSource SecurityDataSource(DatabaseProperties databaseProperties){
        this.url = databaseProperties.getSql().getUrl();
        this.driverClassName = databaseProperties.getSql().getDriverClassName();
        this.username = databaseProperties.getSql().getUsername();
        this.password = databaseProperties.getSql().getPassword();

        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();

        dataSourceBuilder.url(this.url);
        dataSourceBuilder.driverClassName(this.driverClassName);
        dataSourceBuilder.username(this.username);
        dataSourceBuilder.password(this.password);

        return dataSourceBuilder.build();

    }

//    public SecurityDataSource(DatabaseProperties databaseProperties ) {
//
//        this.url = databaseProperties.getSql().getUrl();
//        this.driverClassName = databaseProperties.getSql().getDriverClassName();
//        this.username = databaseProperties.getSql().getUsername();
//        this.password = databaseProperties.getSql().getPassword();
//
//        createAndBuildDataSource();
//
//    }


//    public DataSource createAndBuildDataSource() {
//
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//
//        dataSourceBuilder.url(this.url);
//        dataSourceBuilder.driverClassName(this.driverClassName);
//        dataSourceBuilder.username(this.username);
//        dataSourceBuilder.password(this.password);
//
//        return dataSourceBuilder.build();
//
//    }


    public DataSource createAndBuildDataSource(DatabaseProperties databaseProperties) {

        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();

        dataSourceBuilder.url(databaseProperties.getSql().getUrl());
        dataSourceBuilder.driverClassName(databaseProperties.getSql().getDriverClassName());
        dataSourceBuilder.username(databaseProperties.getSql().getUsername());
        dataSourceBuilder.password(databaseProperties.getSql().getPassword());

        return dataSourceBuilder.build();

    }

}
