package com.gcorp.security.authserver.data.database.datasources;

import org.springframework.boot.jdbc.DataSourceBuilder;

public class MySQLDataSource  {

    public MySQLDataSource(DataSourceBuilder dataSourceBuilder) {
        getMySQLDataSource(dataSourceBuilder);

    }

    public DataSourceBuilder getMySQLDataSource(DataSourceBuilder dataSourceBuilder) {

//        dataSourceBuilder.url(super.url);
//        dataSourceBuilder.driverClassName(super.driverClassName);
//        dataSourceBuilder.username(super.username);
//        dataSourceBuilder.password(super.password);

        return dataSourceBuilder;
    }

}
