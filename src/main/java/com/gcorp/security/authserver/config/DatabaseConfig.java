package com.gcorp.security.authserver.config;

import com.gcorp.security.authserver.annotation.IsNoSQLCondition;
import com.gcorp.security.authserver.annotation.IsSQLCondition;
import com.gcorp.security.authserver.data.database.datasources.SecurityDataSource;
import com.gcorp.security.authserver.properties.DatabaseProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.sql.DataSource;


@Configuration
@DependsOn({"databaseProperties"})
public class DatabaseConfig {


    public static boolean IS_SQL;
    public static boolean IS_NOSQL;


    /**
     * Will autowire DatabaseProperties within this config class
     */
    @Autowired
    private DatabaseProperties databaseProperties;


//    @Bean
//    public DatabaseProperties databaseProperties(){
//
//        return new DatabaseProperties();
//
//    }


    /**
     * Will only create a SQL Database for this project
     * if this is set in the application.properties file
     *
     * @return a DataSource
     */
    @Bean
    @Conditional(IsSQLCondition.class)
    public DataSource getSQLDataSource() {

        SecurityDataSource sds = new SecurityDataSource(databaseProperties);

        DataSource ds = sds.SecurityDataSource(databaseProperties);

        return ds;

    }


    /**
     * Will only create a NoSQL Database for this project
     * if this is set in the application.properties file
     *
     * @return
     */
    @Bean
    @Conditional(IsNoSQLCondition.class)
    @DependsOn("databaseNoSQLProperties")
    public void getNoSQLDataSource() {

        return;

    }

//    @Bean
//    @Conditional(IsSQLCondition.class)
//    public DataSource createAndBuildDataSource() {
//
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//
//        dataSourceBuilder.url(this.databaseProperties.getSql().getUrl());
//        dataSourceBuilder.driverClassName(this.databaseProperties.getSql().getDriverClassName());
//        dataSourceBuilder.username(this.databaseProperties.getSql().getUsername());
//        dataSourceBuilder.password(this.databaseProperties.getSql().getPassword());
//
//        return dataSourceBuilder.build();
//
//    }


}
