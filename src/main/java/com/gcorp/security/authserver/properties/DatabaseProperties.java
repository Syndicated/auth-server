package com.gcorp.security.authserver.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Data
@Component
@ConfigurationProperties(prefix = "database")
public class DatabaseProperties {


    @Value("${type:SQL}")
    private String type;
    @Value("${system:mysql}")
    private String system;
    private SQL sql;


    @Data
    public static class SQL {

        @Value("${sql.url:jdbc:mysql://localhost/security}")
        private String url;
        @Value("${sql.driver-class-name:com.mysql.cj.jdbc.Driver}")
        private String driverClassName;
        @Value("${sql.username:dbadmin}")
        private String username;
        @Value("${sql.password:Password1!}")
        private String password;

    }


}
