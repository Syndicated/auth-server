package com.gcorp.security.authserver.annotation;

import com.gcorp.security.authserver.properties.DatabaseProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.stereotype.Component;

@Component
@DependsOn({"databaseProperties"})
public class IsSQLCondition implements Condition {

    //@Autowired
    DatabaseProperties databaseProperties;

    IsSQLCondition() { }

    @Autowired
    IsSQLCondition(DatabaseProperties databaseProperties) {
        this.databaseProperties = databaseProperties;
    }

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

//        return true;
        return "sql".equalsIgnoreCase(databaseProperties.getType());

    }


}


