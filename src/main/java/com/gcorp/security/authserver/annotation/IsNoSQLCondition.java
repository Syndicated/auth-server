package com.gcorp.security.authserver.annotation;

import com.gcorp.security.authserver.properties.DatabaseProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

//@Configuration
////@ConditionalOnExpression(
////        "${database.type:SQL}"
////)
//@ConditionalOnProperty(
//        value = "database.type",
//        havingValue = "SQL",
//        matchIfMissing = false
//)
//@Configuration
//@ConditionalOnProperty(prefix = "database", name = "type", havingValue = "SQL")
public class IsNoSQLCondition implements Condition {


//    @Autowired
//    DatabaseProperties databaseProperties;


    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

        return false;
        //return "nosql".equalsIgnoreCase(databaseProperties.getType());

    }


}
