package com.gcorp.security.authserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

//@EnableConfigurationProperties(
//		DatabaseProperties.class
//)
@SpringBootApplication(
	exclude={
//                DataSourceAutoConfiguration.class,
//                HibernateJpaAutoConfiguration.class
	}
)
public class AuthServerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AuthServerApplication.class, args);
	}

	@Autowired
	private ApplicationContext appContext;

	@Override
	public void run(String... args) {
		// Get Bean By Name
		Object object = appContext.getBean("isNoSQLCondition");
		System.out.println("Bean object: " + object);
//
//		Object object1 = appContext.getBean("getDatabaseProperties");
//		System.out.println("Bean object: " + object1);

		// Get all Beans
//		String[] beans = appContext.getBeanDefinitionNames();
//		for(String bean:beans){
//			System.out.println("Bean name: " + bean);
//			Object object = appContext.getBean(bean);
//			System.out.println( "Bean object:" + object);
//		}

		// Get the specific type of beans
//		java.util.Map<String,EmployeeService> beans = appContext.getBeansOfType(EmployeeService.class);
//		beans.forEach((k,v)->{
//			System.out.println(k + " - "+v);
//		});

		// Get the specific type of beans by annotation type
//		java.util.Map<String,Object> beans = appContext.getBeansWithAnnotation(Configuration.class);
//		beans.forEach((k,v)->{
//			System.out.println(k + " - "+v);
//		});

	}


}
